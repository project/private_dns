Private DNS
===========


# What is this

This module aims to provide a private DNS system for your own IP based network.

# What's for

# Use cases:

I don't want to share my private info on public DNS service;
I don't want to spend money on private IP server  and I want to access to my office computer from home where I have a dynamic IP internet connection;
My test/dev web server is behind a dynamic public IP. I need to give access to testers/developers to the server without buying another VPS/hosting?
I want a direct access to my remote web camera without sharing my data on more external servers.


# How to use PRIVATE DNS
. install module (composer is suggested)
. get customized scripts from "Configure > Private DNS"
. install and crontab scripts from your machines

Once done, you will be always able to access to your remote internet machines, even when behind public address.


Project page: https://www.drupal.org/project/private_dns
Docs: https://www.drupal.org/docs/contributed-modules/private-dns-for-your-dynamic-ip-internet-connection

Supporting organization:  Fagioli.biz



