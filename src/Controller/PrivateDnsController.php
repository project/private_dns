<?php

namespace Drupal\private_dns\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @file
 * Contains \Drupal\private_dns\Controller\PrivateDnsController.
 */

class PrivateDnsController {

  public function set ($ip , $name , $key) {

    if ( private_dns_validate_key($key, "g") == false ) {
      echo "Err:43gh";
      exit;
    }

    $timestamp = private_dns_get_timestamp([]);
    $args   = private_dns_get_args();
    $ip     = $args[2];
    $name   = $args[3];
    $key    = $args[4];

    private_dns_validate_key($key, "s");
    private_dns_validate_ip($ip, "s");

    \Drupal::state()->set('private_dns_ip_0', $ip);
    \Drupal::state()->set('private_dns_name_0', $name);
    \Drupal::state()->set('private_dns_key_0', $key);
    \Drupal::state()->set('private_dns_timestamp_0', $timestamp);
    echo "Thanks!";    exit;
  }

  public function get ($ip , $name , $key) {

    if ( private_dns_validate_key($key, "g") == false ) {
      echo "Err:43gh";
      exit;
    }

    $data = [];
    $data['ip']   = \Drupal::state()->get('private_dns_ip_0', null);
    $data['name'] = \Drupal::state()->get('private_dns_name_0', null);
    $data['key']  = \Drupal::state()->get('private_dns_key_0', null);
    echo $data['ip'];
    exit;
  }

  /*
  * Main info page
  */
  public function create_file_hosts_script () {

    //never cache this page
    \drupal::service('page_cache_kill_switch')->trigger();

    $host = \drupal::request()->gethost();
    $domain_with_schema = \drupal::request()->getschemeandhttphost();
    $args = private_dns_get_args();

    if (!isset( $args[2])) {
      $args[2] = "-";
    }

    $output = '' ;

    if ((\Drupal::state()->get('private_dns_timestamp_0', null) )) {
      $output .= "This is your actual Private DNS table:";
      $output .= "<table id='private-dns-table'>";
      $output .= "<tr>";
      $output .= "<th>IP</th>";
      $output .= "<th>name</th>";
      $output .= "<th>updated on </th>";
      $output .= "<tr>";
      $output .= "<td>" . \Drupal::state()->get('private_dns_ip_0', null); ;
      $output .= "<td>" . \Drupal::state()->get('private_dns_name_0', null);     ;
      $output .= "<td>" . \Drupal::state()->get('private_dns_timestamp_0', null);  ;
      $output .= "</th>";
      $output .= "</table>";
    }
    else  {
      $output .= "<h3>Welcome to your fresh new private_dns</h3>Update your table via browser or script . ";
    }

    $output .= '<h2>' . t('Set up') . '</h2>'  ;
    $output .= '<h3><b>A.</b> '.t('Send address from your dynamic IP connection .', []).'</h3>';
    $output .= "<p>Send your dynamic IP to <i>this very drupal installation</i> with the script from your local machine behind the dynamic IP internet connection.</p>";
    $output .= '<h4>' . t('Via Browser') . '</h4>'  ;
    $output .= "<code>   " . $domain_with_schema . PRIVATE_DNS_BASE_CALL  . "/set/SOME-IP/SOME-NAME/".PRIVATE_DNS_KEY . " </code><br/>";
    $output .= '<h4>' . t('Via Script') . '</h4>'  ;
    $output .= "<p>Paste the code below in <a href='https://www.drupal.org/node/3192512'>the script</a> for the machine behind the dynamic IP</p>";
    $output .= "<code>KEY=\"".PRIVATE_DNS_KEY . "\";</code><br/>";
    $output .= "<code>DRUPAL_WEBSITE=\"" . $domain_with_schema . "\";</code> #<i>this very drupal installation</i><br/>";
    $output .= "<code>BASE_CALL=\"" . PRIVATE_DNS_BASE_CALL  . "\";</code><br/>";
    $output .= "<p>Running the script above via crontab, the table above will be regularry updated with your dynamic IP.</p>";
    $output .= '<h3> <b>B.</b> ' . t('Update your machine /etc/hosts.') . '</h3>';
    $output .= '<h4>' . t('Via Browser') . '</h4>'  ;
    $output .= "<code>  " . $domain_with_schema . PRIVATE_DNS_BASE_CALL  . "/get/SOME-NAME/".PRIVATE_DNS_KEY . " </code><br/>";
    $output .= '<h4>' . t('Via Script') . '</h4>'  ;
    $output .= "<p>Update the hosts file of the machine/s in need of the IP, with <a href='https://www.drupal.org/node/3192506'>the script</a>
    updated with the code below.</p>";
    $output .= "<code>KEY=\"".PRIVATE_DNS_KEY . "\";</code><br/>";
    $output .= "<code>DRUPAL_WEBSITE=\"" . $domain_with_schema . "\";</code><br/>";
    $output .= "<code>BASE_CALL=\"" . PRIVATE_DNS_BASE_CALL  . "\";</code><br/>";
    $output .= "<p>Running the script above via crontab, your remote machine will be updated with your last dynamic IP.</p>";
    $output .= "<p>Have fun!</p>";
    return [
      '#type' => 'markup',
      '#markup' => $output,
    ];
  }

}
